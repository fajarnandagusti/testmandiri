package com.fajarnandagusti.testassesmentbankmandiri.activity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.fajarnandagusti.testassesmentbankmandiri.R
import com.fajarnandagusti.testassesmentbankmandiri.base.BaseActivity
import com.fajarnandagusti.testassesmentbankmandiri.base.BaseFragment
import com.fajarnandagusti.testassesmentbankmandiri.base.OnBackPressListener
import com.fajarnandagusti.testassesmentbankmandiri.base.OnPageInteraction
import com.fajarnandagusti.testassesmentbankmandiri.databinding.ActivityMainBinding
import com.fajarnandagusti.testassesmentbankmandiri.fragment.MainMenuFragment
import com.fajarnandagusti.testassesmentbankmandiri.utils.UtilsHelper

class MainActivity : BaseActivity(), OnPageInteraction {
    private lateinit var binding: ActivityMainBinding

    override fun getContentView(): Int {
        return R.layout.activity_main
    }

    override fun bindingView() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
    }

    override fun setTitlePage(title: String) {
        binding.appbar.textTitle.text = title
    }


    override fun addFragment(fragment: BaseFragment) {
        supportFragmentManager.beginTransaction().apply {
//            setCustomAnimations(
//                R.anim.enter_from_bottom_to_top,
//                R.anim.exit_from_top_to_bottom,
//                R.anim.fade_in,
//                R.anim.fade_out
//            )
            replace(R.id.frameLayout, fragment)
            addToBackStack(null)
            commit().apply {
                setReorderingAllowed(true)
            }
        }
        mainViewCondition(fragment)
    }

    override fun onBackPressed(onback: OnBackPressListener) {
        onBackPressed()
        onback.onBackPressed(this)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        UtilsHelper.hideSoftKeyboard(this)

        val fragment = supportFragmentManager.findFragmentById(R.id.frameLayout) as BaseFragment
        setTitlePage(fragment.getTitlePage())
        mainViewCondition(fragment)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.appbar.ibBack.setOnClickListener { onBackPressed() }

//        binding.appbar.textTitle.visibility = View.GONE

        binding.appbar.ibBack.visibility = View.GONE
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.frameLayout, MainMenuFragment())
            commit()
        }

    }


    private fun mainViewCondition(fragment: Fragment){
        if(fragment is MainMenuFragment)
            binding.appbar.ibBack.visibility = View.GONE
        else
            binding.appbar.ibBack.visibility = View.VISIBLE
    }

}