package com.fajarnandagusti.testassesmentbankmandiri.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fajarnandagusti.testassesmentbankmandiri.BuildConfig
import com.fajarnandagusti.testassesmentbankmandiri.R
import com.fajarnandagusti.testassesmentbankmandiri.base.MyFragment
import com.fajarnandagusti.testassesmentbankmandiri.databinding.FragmentMainMenuBinding
import com.fajarnandagusti.testassesmentbankmandiri.databinding.ItemCategoryBinding
import com.fajarnandagusti.testassesmentbankmandiri.model.CategoryMenu
import com.fajarnandagusti.testassesmentbankmandiri.utils.GenericAdapter

open class MainMenuFragment: MyFragment() {

    private lateinit var binding:FragmentMainMenuBinding
    private lateinit var adapter: GenericAdapter<CategoryMenu>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main_menu, container, false)
        return binding.root
    }

    override fun getTitlePage(): String {
        return getString(R.string.category)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = object : GenericAdapter<CategoryMenu>(){
            override fun getViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
                return VHAdapterMenu(DataBindingUtil.inflate(layoutInflater, R.layout.item_category, parent, false))
            }
        }

        adapter.setData(getMainMenu())
        binding.recyclerview.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
        }.also { it.adapter = adapter }

    }


    inner class VHAdapterMenu(private val binding: ItemCategoryBinding): RecyclerView.ViewHolder(binding.root), GenericAdapter.Bind<CategoryMenu>{
        override fun bind(item: CategoryMenu) {
            binding.data =item
            itemView.setOnClickListener {
                onClickMenu(item)
            }
        }
    }


    open fun onClickMenu(item:CategoryMenu){
        listener?.addFragment(SourceNewsFragment.getInstance(item.menuName))
    }


}