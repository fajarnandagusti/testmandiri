package com.fajarnandagusti.testassesmentbankmandiri.fragment

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView.OnEditorActionListener
import android.widget.Toast
import androidx.core.widget.NestedScrollView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fajarnandagusti.testassesmentbankmandiri.BuildConfig
import com.fajarnandagusti.testassesmentbankmandiri.R
import com.fajarnandagusti.testassesmentbankmandiri.base.MyFragment
import com.fajarnandagusti.testassesmentbankmandiri.databinding.FragmentArticleBinding
import com.fajarnandagusti.testassesmentbankmandiri.databinding.ItemArticleNewsBinding
import com.fajarnandagusti.testassesmentbankmandiri.model.Articles
import com.fajarnandagusti.testassesmentbankmandiri.model.ListArticles
import com.fajarnandagusti.testassesmentbankmandiri.utils.GenericAdapter
import com.fajarnandagusti.testassesmentbankmandiri.utils.MyCallback
import retrofit2.Call


class ArticleFragment : MyFragment() {
    private lateinit var binding: FragmentArticleBinding
    private lateinit var adapter: GenericAdapter<Articles>
    private var sourceNews:String?=null

    private var keyword:String = ""

    private var pages = 1
    private var pageSize = 20
    private var isEmpty = false
    private var listData: ArrayList<Articles>? = null

    companion object{

        fun getInstance(sourceNews:String): ArticleFragment {
            ArticleFragment().apply {
                arguments = Bundle().apply {
                    putString("sourceNews", sourceNews)
                }
            }.also { return it }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_article, container, false)
        return binding.root
    }

    override fun getTitlePage(): String {
        return getString(R.string.article)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.handler = this
        sourceNews = requireArguments().getString("sourceNews")

        binding.edtSearch.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                pages = 1
                listData?.clear()
                keyword = binding.edtSearch.text.toString()
                getData()
                return@OnEditorActionListener true
            }
            false
        })


        //init adapter data list
        adapter = object : GenericAdapter<Articles>() {
            override fun getViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
                return VHAdapterArticleNews(
                    DataBindingUtil.inflate(
                        layoutInflater,
                        R.layout.item_article_news,
                        parent,
                        false
                    )
                )
            }
        }

        //init recyclerview
        binding.recyclerview.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
        }.also { it.adapter = adapter }

        //retrieve data 
        getData()

        //listenr to the list view
        binding.nestedView.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, _, scrollY, _, oldScrollY ->
            if (v!!.getChildAt(v.childCount - 1) != null) {
                if (scrollY >= v.getChildAt(v.childCount - 1).measuredHeight - v.measuredHeight && scrollY > oldScrollY) {
                    if (!isEmpty) {
                        binding.progressLoad.visibility = View.VISIBLE
                        pages++
                        getData()
                    }

                }
            }
        })

    }

    private fun getData() {
        //show loading full screen when page = 0. Next page, only show loading progress in bottom
        if (pages == 1) {
            showLoading()
        }

        Handler().postDelayed({
            callAPI(api.getArticle(BuildConfig.API_KEY, sourceNews.toString(), pages, pageSize, keyword), object : MyCallback<ListArticles>(this){
                override fun onSuccess(body: ListArticles?) {


                    body?.let {

                        if (pages == 1) {
                            listData = it.articles
                            if (listData.isNullOrEmpty()) {
                                binding.recyclerview.visibility = View.GONE
                                binding.progressLoad.visibility = View.GONE
                                binding.nodata.visibility = View.VISIBLE
                                isEmpty = true

                            }else{
                                adapter.setData(listData)
                                binding.recyclerview.visibility = View.VISIBLE
                                binding.progressLoad.visibility = View.GONE
                                binding.nodata.visibility = View.GONE
                            }
                        }else{
                            binding.progressLoad.visibility = View.GONE
                            if (it.articles.isNullOrEmpty()) {

                                isEmpty = true

                            } else {

                                listData?.addAll(it.articles.toList())
                                adapter.setData(listData)
                            }

                        }


                    }
                }


                override fun onFailed(call: Call<ListArticles>, t: Throwable) {
                    isEmpty = true
                    binding.recyclerview.visibility = View.GONE
                    binding.progressLoad.visibility = View.GONE
                    binding.nodata.visibility = View.VISIBLE

                    Toast.makeText(
                        context,
                        getString(R.string.unable_connect),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            )
        }, 250)
    }

    inner class VHAdapterArticleNews(private val binding: ItemArticleNewsBinding) :
        RecyclerView.ViewHolder(binding.root), GenericAdapter.Bind<Articles> {
        override fun bind(item: Articles) {
            binding.data = item

            itemView.setOnClickListener {
                Log.e("URL",item.url.toString())
                listener?.addFragment(ReadArticleFragment.getInstance(item.url.toString()))
            }


        }
    }

}