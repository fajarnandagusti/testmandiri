package com.fajarnandagusti.testassesmentbankmandiri.base

import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.facebook.shimmer.ShimmerFrameLayout
import com.fajarnandagusti.testassesmentbankmandiri.MyApp
import com.fajarnandagusti.testassesmentbankmandiri.R
import com.fajarnandagusti.testassesmentbankmandiri.model.BaseModel
import com.fajarnandagusti.testassesmentbankmandiri.model.CategoryMenu
import com.fajarnandagusti.testassesmentbankmandiri.service.MyAPIService
import com.fajarnandagusti.testassesmentbankmandiri.utils.MyCallback
import retrofit2.Call
import retrofit2.Callback

abstract class BaseFragment : Fragment(), BaseUI{
    private var mDialog: Dialog?=null
    private var mFm: FragmentManager?=null
    private var mShimmering: ShimmerFrameLayout?=null
    private var mNodataView: TextView?=null

    override val ctx: Context?
        get() = context

    override val dialog: Dialog?
        get() = mDialog
    override val fm: FragmentManager?
        get() = mFm
    override val shimmeringLoading: ShimmerFrameLayout?
        get() = mShimmering

    override val api: MyAPIService
        get() = MyApp.getApi()


    override val noDataView: TextView?
        get() = mNodataView


    override fun <T : BaseModel> callAPI(call: Call<T>, callback: MyCallback<T>) {
        MyApp.callApi(this, call, callback)
    }

    override fun <T> defaultCallAPI(call: Call<T>, callback: Callback<T>) {
        MyApp.callApi(this, call, callback)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mFm = childFragmentManager
        mDialog = Dialog(ctx!!)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        mShimmering = view.findViewById(R.id.shimmeringLayout)
//        mNodataView = view.findViewById(R.id.noData)

        mShimmering?.startShimmer()
    }

    open fun getTitlePage() = getString(R.string.app_name)

    fun showLoading(msg:String?){
        mDialog?.setContentView(R.layout.dialog_loading)
        mDialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val textMessage = mDialog?.findViewById<TextView>(R.id.textLoading)

        if(msg!= null)
            textMessage?.text = msg?:""

        mDialog?.show()
    }

    fun showLoading(){
        showLoading(null)
    }

    fun dismissLoading(){
        mDialog?.dismiss()
    }




    protected fun getMainMenu():List<CategoryMenu>{
        val menuList = ArrayList<CategoryMenu>()

        menuList.add(CategoryMenu( getString(R.string.business), true))
        menuList.add(CategoryMenu( getString(R.string.general), true))
        menuList.add(CategoryMenu( getString(R.string.health), true))
        menuList.add(CategoryMenu( getString(R.string.technology), true))
        menuList.add(CategoryMenu( getString(R.string.sports), true))
        menuList.add(CategoryMenu( getString(R.string.entertainment), true))
        menuList.add(CategoryMenu( getString(R.string.science), true))

        return menuList.toList()
    }

}