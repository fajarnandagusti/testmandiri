package com.fajarnandagusti.testassesmentbankmandiri.base

import android.app.Dialog
import android.content.Context
import android.widget.TextView
import androidx.fragment.app.FragmentManager

import com.facebook.shimmer.ShimmerFrameLayout
import com.fajarnandagusti.testassesmentbankmandiri.model.BaseModel
import com.fajarnandagusti.testassesmentbankmandiri.service.MyAPIService
import com.fajarnandagusti.testassesmentbankmandiri.utils.MyCallback
import retrofit2.Call
import retrofit2.Callback

interface BaseUI {

    val ctx: Context?
    val dialog: Dialog?
    val fm: FragmentManager?
    val shimmeringLoading: ShimmerFrameLayout?
    val api: MyAPIService
    val noDataView: TextView?


    fun <T : BaseModel> callAPI(call: Call<T>, callback: MyCallback<T>)
    fun <T> defaultCallAPI(call: Call<T>, callback: Callback<T>)
}