package com.fajarnandagusti.testassesmentbankmandiri.fragment

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fajarnandagusti.testassesmentbankmandiri.BuildConfig
import com.fajarnandagusti.testassesmentbankmandiri.R
import com.fajarnandagusti.testassesmentbankmandiri.base.MyFragment
import com.fajarnandagusti.testassesmentbankmandiri.databinding.FragmentSourceNewsBinding
import com.fajarnandagusti.testassesmentbankmandiri.databinding.ItemSourceNewsBinding
import com.fajarnandagusti.testassesmentbankmandiri.model.Sources
import com.fajarnandagusti.testassesmentbankmandiri.model.ListSource
import com.fajarnandagusti.testassesmentbankmandiri.utils.GenericAdapter
import com.fajarnandagusti.testassesmentbankmandiri.utils.MyCallback
import retrofit2.Call

class SourceNewsFragment : MyFragment() {
    private lateinit var binding: FragmentSourceNewsBinding
    private lateinit var adapter: GenericAdapter<Sources>

    private var categoryNews:String?=null

    companion object{

        fun getInstance(category:String): SourceNewsFragment {
            SourceNewsFragment().apply {
                arguments = Bundle().apply {
                    putString("category", category)
                }
            }.also { return it }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_source_news, container, false)
        return binding.root
    }

    override fun getTitlePage(): String {
        return getString(R.string.source)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.handler = this
        categoryNews = requireArguments().getString("category")
        getSourceByCategory(categoryNews.toString())

        adapter = object : GenericAdapter<Sources>(){
            override fun getViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
                return VHAdapterMenu(DataBindingUtil.inflate(layoutInflater, R.layout.item_source_news, parent, false))
            }
        }

        binding.recyclerview.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
        }.also { it.adapter = adapter }

    }

    private fun getSourceByCategory(category: String) {
        Handler().postDelayed({
            showLoading()
            callAPI(api.getSourceByCategory(BuildConfig.API_KEY, category), object : MyCallback<ListSource>(this){
                override fun onSuccess(body: ListSource?) {
                    body?.let {

                        if (it.sources.isNullOrEmpty()) {
                            binding.recyclerview.visibility = View.GONE
                            binding.noData.visibility = View.VISIBLE
                        }else
                            binding.recyclerview.visibility = View.VISIBLE
                            binding.noData.visibility = View.GONE

                            adapter.setData(it.sources)

                    }
                }

                override fun onFailed(call: Call<ListSource>, t: Throwable) {
                    Toast.makeText(
                        context,
                        getString(R.string.unable_connect),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            )
        }, 250)

    }


    inner class VHAdapterMenu(private val binding: ItemSourceNewsBinding): RecyclerView.ViewHolder(binding.root), GenericAdapter.Bind<Sources>{
        override fun bind(item: Sources) {
            binding.data =item
            itemView.setOnClickListener {
                listener?.addFragment(ArticleFragment.getInstance(item.id.toString()))
//                onClickMenu(item)
            }
        }
    }
}