package com.fajarnandagusti.testassesmentbankmandiri.custom

import android.content.Context
import android.content.res.TypedArray
import android.text.InputFilter
import android.text.InputType
import android.text.TextWatcher
import android.text.method.DigitsKeyListener
import android.util.AttributeSet
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.TextView
import com.fajarnandagusti.testassesmentbankmandiri.R

class MyInputEditTextView:MyInputTextView {

    private var inputType: Int=0
    private lateinit var typedArray: TypedArray

    constructor(context: Context):super(context, null){init(context, null)}

    constructor(context: Context, attributeSet: AttributeSet?):super(context, attributeSet){init(context, attributeSet)}

    private fun init(context: Context, attributeSet: AttributeSet?){
        typedArray = context.obtainStyledAttributes(attributeSet, R.styleable.MyInputEditTextView,R.attr.myInputViewStyle,R.style.MyInputView)

        inputType=typedArray.getInt(R.styleable.MyInputEditTextView_android_inputType, InputType.TYPE_CLASS_TEXT)
        textView!!.inputType = inputType
        textView!!.imeOptions = EditorInfo.IME_ACTION_DONE

    }

    fun setMaxLength(amount:Int){
        textView!!.filters += InputFilter.LengthFilter(amount)
    }

    override fun getInputView(): TextView? {
        return EditText(context)
    }

    fun addTextChangedListener(textWatcher: TextWatcher) {
        textView?.addTextChangedListener(textWatcher)
    }

    fun setFilterEmojis(){
        textView?.filters = EmojiFilter.filter;
    }

    fun setFilterSpecialChar(){
        textView?.keyListener = DigitsKeyListener.getInstance("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz 1234567890.,")
        textView?.setRawInputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME)
    }

    object EmojiFilter {
        val filter: Array<InputFilter>
            get() {
                val EMOJI_FILTER =
                    InputFilter { source, start, end, dest, dstart, dend ->
                        for (index in start until end) {
                            val type = Character.getType(source[index])
                            if (type == Character.SURROGATE.toInt() || type == Character.NON_SPACING_MARK.toInt() || type == Character.OTHER_SYMBOL.toInt()) {
                                return@InputFilter ""
                            }
                        }
                        null
                    }
                return arrayOf(EMOJI_FILTER)
            }
    }
}