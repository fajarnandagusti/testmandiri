package com.fajarnandagusti.testassesmentbankmandiri.utils

import android.app.Activity
import android.content.Context
import android.graphics.*
import android.view.View
import android.view.inputmethod.InputMethodManager

class UtilsHelper {

    companion object{

        fun hideSoftKeyboard(a: Activity) {
            val view = a.currentFocus
            if (view != null) {
                val imm = a.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view.windowToken, 0)
            }
        }

        fun hideKeyboard(view: View){
            val inputMethodManager = view.context
                .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
        }


    }
}