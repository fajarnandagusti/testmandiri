package com.fajarnandagusti.testassesmentbankmandiri.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ListArticles : BaseModel() {
    @SerializedName("articles")
    @Expose
    val articles: ArrayList<Articles>? = null
}