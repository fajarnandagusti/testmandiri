package com.fajarnandagusti.testassesmentbankmandiri.custom

import android.content.Context
import android.content.res.TypedArray
import android.graphics.drawable.Drawable
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TableRow
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import com.fajarnandagusti.testassesmentbankmandiri.R

open class MyInputTextView: TableRow {

    private var mTextAlignment:Int= View.TEXT_ALIGNMENT_TEXT_START
    private lateinit var root: ViewGroup

    private lateinit var typedArray: TypedArray

    var text: String
        get() {
            return textView?.text.toString()
        }
        set(value) {
            textView?.text = value
        }

    var mBackground: Drawable
        get() {
            return ContextCompat.getDrawable(context, R.drawable.bg_edittext_input)!!
        }
        set(value){
            textView?.background = value
        }

    private var label: TextView?=null
    var hint:CharSequence?=null
    protected var textView: TextView?=null

    constructor(context: Context, attrs: AttributeSet?) :
            super(context,attrs){
        init(context,attrs)
    }
    constructor(context: Context) : this(context,null){
        init(context,null)
    }

    private fun init(context: Context, attrs: AttributeSet?) {
        typedArray = context.obtainStyledAttributes(attrs, R.styleable.MyInputTextView)
        hint = typedArray.getText(R.styleable.MyInputTextView_android_hint)
        mTextAlignment = typedArray.getInt(R.styleable.MyInputTextView_android_textAlignment, View.TEXT_ALIGNMENT_TEXT_START)
        root = LinearLayout(context)
        (root as LinearLayout).orientation = LinearLayout.VERTICAL
        (root as LinearLayout).isDuplicateParentStateEnabled = true
        root.layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
        addView(root)

        if (!hint.isNullOrEmpty()) {
            addLabel()
        }

        textView = getInputView()
        textView?.textAlignment = mTextAlignment

        val layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        textView!!.layoutParams = layoutParams

        val paddingValue = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16f, resources.displayMetrics).toInt()
        val paddingValueUpDown = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 13f, resources.displayMetrics).toInt()

        textView?.setPadding(
            paddingValue,
            paddingValueUpDown,
            paddingValue,
            paddingValueUpDown)
        textView?.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f)
        textView?.setTextColor(ContextCompat.getColor(context, R.color.black))
        textView?.background = mBackground

        root.addView(textView)
        typedArray.recycle()

    }

    fun setEndIcon(drawable: Drawable?){
        textView?.setCompoundDrawablesWithIntrinsicBounds(null,null,drawable, null)
    }

    protected open fun getInputView(): TextView? {
        return TextView(context)
    }


    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        if(root!=this)
            root.isEnabled=enabled
        label?.isEnabled=enabled
        textView?.isEnabled=enabled
    }


    private fun addLabel() {
        label= TextView(context)
        label!!.setTextColor(ContextCompat.getColor(context, R.color.black))
        label!!.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f)

        val lp = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
        lp.bottomMargin = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,8f, resources.displayMetrics).toInt()
        label!!.layoutParams= lp
//        label!!.text=String.format("%s%s", hint,if(isTopLabel) "" else "")
        label!!.text= hint


        root.addView(label)
    }

    companion object{

        @BindingAdapter(value = ["android:textAttrChanged"])
        @JvmStatic fun setListener(view:MyInputTextView,  listener: InverseBindingListener?) {
            if (listener != null) {
                view.textView?.addTextChangedListener(object: TextWatcher {
                    override fun afterTextChanged(s: Editable?) {
                        listener.onChange()
                    }

                    override fun beforeTextChanged(s: CharSequence?,start: Int,count: Int,after: Int) {}

                    override fun onTextChanged(s: CharSequence?,start: Int,before: Int,count: Int) {}
                });
            }
        }
        @BindingAdapter("android:text")
        @JvmStatic fun setText(view: MyInputTextView, newValue: String?) {
            if (view.text != newValue) {
                view.text = newValue?:""
            }
        }
        @InverseBindingAdapter(attribute = "android:text")
        @JvmStatic fun getText(view: MyInputTextView) : String {
            return view.text
        }

    }
}