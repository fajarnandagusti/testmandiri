package com.fajarnandagusti.testassesmentbankmandiri.model

    data class CategoryMenu(
        var menuName:String,
        var isSelected: Boolean
    )
