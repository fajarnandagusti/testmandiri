package com.fajarnandagusti.testassesmentbankmandiri.utils

import android.view.View
import android.widget.Toast
import com.fajarnandagusti.testassesmentbankmandiri.base.BaseUI
import com.fajarnandagusti.testassesmentbankmandiri.custom.BottomDialogError
import com.fajarnandagusti.testassesmentbankmandiri.model.BaseModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

abstract class MyCallback<T: BaseModel>(private val baseUI: BaseUI): Callback<T> {

    override fun onResponse(call: Call<T>, response: Response<T>) {
        //when Dialog() is show up, everytime service response code 200, auto dismiss
        baseUI.dialog?.dismiss()

        if(response.code() == 200){
            //call on success callback if response code server 200
            baseUI.shimmeringLoading?.stopShimmer()
            baseUI.shimmeringLoading?.visibility = View.GONE

            if(response.isSuccessful){
                onSuccess(response.body())
            }
            else
                Toast.makeText(baseUI.ctx, " [${response.body()?.status?:""}] ${response.body()?.message?:""}", Toast.LENGTH_SHORT).show()


        }else{

            //check if there is dialog popup still on window, dismiss it
//            baseUI.dialog?.dismiss()
            //show error into bottom dialog
            BottomDialogError.init(response.code().toString(), response.message(), response.errorBody()?.string()?:"-", baseUI.fm)
        }

    }

    override fun onFailure(call: Call<T>, t: Throwable) {
        baseUI.dialog?.dismiss()

        onFailed(call, t)
    }

    protected abstract fun onSuccess(body:T?)

    protected abstract fun onFailed(call: Call<T>, t:Throwable)

}