package com.fajarnandagusti.testassesmentbankmandiri.base

import android.content.Context

open class MyFragment:BaseFragment() {

    protected var listener:OnPageInteraction?=null

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if(context is OnPageInteraction){
            listener=context
        }else{
            throw RuntimeException("$context must implement OnPageInteraction")
        }
    }

    override fun onResume() {
        super.onResume()
        listener?.setTitlePage(getTitlePage())
    }
}