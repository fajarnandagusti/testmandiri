package com.fajarnandagusti.testassesmentbankmandiri.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.WindowInsets
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import com.fajarnandagusti.testassesmentbankmandiri.R
import com.fajarnandagusti.testassesmentbankmandiri.base.BaseActivity
import com.fajarnandagusti.testassesmentbankmandiri.databinding.ActivitySplashBinding

@SuppressLint("CustomSplashScreen")
class SplashActivity : BaseActivity() {

    private lateinit var binding: ActivitySplashBinding
    override fun getContentView() = R.layout.activity_splash
    override fun bindingView() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.handler = this
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
            )
        } else {
            window.insetsController?.hide(WindowInsets.Type.statusBars())
        }

        Handler().postDelayed({

            Intent(applicationContext, MainActivity::class.java).let { intnt ->

                val intentBundle = intent
                intentBundle.extras.let { bundle ->
                    if (bundle != null) {
                        intnt.putExtras(bundle)
                    }
                }

                startActivity(intnt)
                finish()
            }

        }, 1000)
    }

}