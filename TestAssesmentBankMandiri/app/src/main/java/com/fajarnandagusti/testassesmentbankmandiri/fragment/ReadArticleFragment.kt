package com.fajarnandagusti.testassesmentbankmandiri.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.databinding.DataBindingUtil
import com.fajarnandagusti.testassesmentbankmandiri.R
import com.fajarnandagusti.testassesmentbankmandiri.base.MyFragment
import com.fajarnandagusti.testassesmentbankmandiri.databinding.FragmentReadArticleBinding

class ReadArticleFragment : MyFragment() {

    private lateinit var binding:FragmentReadArticleBinding
    private var urlNews:String?=null

    companion object{

        fun getInstance(urlNews:String): ReadArticleFragment {
            ReadArticleFragment().apply {
                arguments = Bundle().apply {
                    putString("urlNews", urlNews)
                }
            }.also { return it }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_read_article, container, false)
        return binding.root
    }

    override fun getTitlePage(): String {
        return getString(R.string.read)
    }


    @SuppressLint("SetJavaScriptEnabled")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        urlNews = requireArguments().getString("urlNews")

        showLoading()
        binding.webview.let { web ->

            web.settings.javaScriptEnabled = true
            web.settings.loadWithOverviewMode = true
            web.settings.domStorageEnabled = true
            web.settings.useWideViewPort = true
            web.settings.builtInZoomControls = true
            web.webViewClient = object : WebViewClient(){

                override fun onPageFinished(view: WebView?, url: String?) {
                    super.onPageFinished(view, url)
                    dismissLoading()
                }

                override fun shouldOverrideUrlLoading(
                    view: WebView?,
                    request: WebResourceRequest?
                ): Boolean {
//                    dismissLoading()
                    web.loadUrl(request?.url.toString())
                    return super.shouldOverrideUrlLoading(view, request)
                }
            }
            web.loadUrl(urlNews?:"")
        }

    }



}