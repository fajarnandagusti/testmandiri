package com.fajarnandagusti.testassesmentbankmandiri.custom

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentManager
import com.fajarnandagusti.testassesmentbankmandiri.R
import com.fajarnandagusti.testassesmentbankmandiri.databinding.BottomSheetErrorBinding
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class BottomDialogError(val errCode:String?, val message:String?, val bodyResponse:String?):
    BottomSheetDialogFragment() {

    companion object{

        fun init(errCode: String?,message: String?,bodyResponse: String?,fm: FragmentManager?):BottomDialogError{
            BottomDialogError(errCode, message, bodyResponse).let {
                it.showNow(fm!!, "err_msg")
                it.setOnClickListener(object : BottomDialogErrorListener{
                    override fun onCloseDialog() {
                        it.dismiss()
                    }
                })
                return it
            }
        }
    }

    private lateinit var binding: BottomSheetErrorBinding
    private var listener:BottomDialogErrorListener?=null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.bottom_sheet_error, container, false)
        return binding.root
    }

    fun setOnClickListener(listener: BottomDialogErrorListener){
        this.listener = listener
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.handler         = this
        binding.errCode         = errCode
        binding.errMessage      = message
        binding.bodyResponse    = bodyResponse
    }

    fun closeDialog(view: View){
        listener?.onCloseDialog()
    }

    interface BottomDialogErrorListener{
        fun onCloseDialog()
    }

}