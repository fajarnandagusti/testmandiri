package com.fajarnandagusti.testassesmentbankmandiri.base

import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import com.fajarnandagusti.testassesmentbankmandiri.model.BaseModel
import com.facebook.shimmer.ShimmerFrameLayout
import com.fajarnandagusti.testassesmentbankmandiri.MyApp
import com.fajarnandagusti.testassesmentbankmandiri.R
import com.fajarnandagusti.testassesmentbankmandiri.service.MyAPIService
import com.fajarnandagusti.testassesmentbankmandiri.utils.MyCallback
import com.fajarnandagusti.testassesmentbankmandiri.utils.UtilsHelper
import retrofit2.Call
import retrofit2.Callback

abstract class BaseActivity: AppCompatActivity(), BaseUI {

    private var mDialog: Dialog?=null
    private var mFm: FragmentManager?=null
    private var mShimmering: ShimmerFrameLayout?=null
    private var mNodataView: TextView?=null

    override val ctx: Context?
        get() = this
    override val dialog: Dialog?
        get() = mDialog
    override val fm: FragmentManager?
        get() = mFm
    override val shimmeringLoading: ShimmerFrameLayout?
        get() = mShimmering

    override val api: MyAPIService
        get() = MyApp.getApi()

    override val noDataView: TextView?
        get() = mNodataView


    override fun <T : BaseModel> callAPI(call: Call<T>, callback: MyCallback<T>) {
        MyApp.callApi(this, call, callback)
    }

    override fun <T> defaultCallAPI(call: Call<T>, callback: Callback<T>) {
        MyApp.callApi(this, call, callback)
    }

    protected open fun bindingView(){
        setContentView(getContentView())
    }

    protected abstract fun getContentView():Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindingView()
        mFm = supportFragmentManager
        mDialog = Dialog(this)

//        mNodataView = findViewById(R.id.noData)

    }


    fun showLoading(){
        showLoading(null)
    }


    fun dismissLoading(){
        mDialog?.dismiss()
    }


    fun showLoading(msg:String?){
        mDialog?.setContentView(R.layout.dialog_loading)
        mDialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val textMessage = mDialog?.findViewById<TextView>(R.id.textLoading)

        if(msg!= null)
            textMessage?.text = msg?:""

        mDialog?.show()
    }



    override fun onDestroy() {
        super.onDestroy()
        UtilsHelper.hideSoftKeyboard(this)
        mDialog?.dismiss()
    }

}