package com.fajarnandagusti.testassesmentbankmandiri.model

import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import com.fajarnandagusti.testassesmentbankmandiri.utils.CommonUtils
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import com.fajarnandagusti.testassesmentbankmandiri.R
import java.text.SimpleDateFormat
import java.util.*

class Articles (
    @SerializedName("author")
    @Expose
    val author: String? = null,

    @SerializedName("title")
    @Expose
    val title: String? = null,

    @SerializedName("description")
    @Expose
    val description: String? = null,

    @SerializedName("url")
    @Expose
    val url: String? = null,

    @SerializedName("urlToImage")
    @Expose
    val urlToImage: String? = null,

    @SerializedName("publishedAt")
    @Expose
    val publishedAt: String? = null,

    @SerializedName("content")
    @Expose
    val content: String? = null,
        ){

    companion object{
        @BindingAdapter("imgCover")
        @JvmStatic fun imgCover(view: AppCompatImageView, url:String?){
            CommonUtils.loadAppCompatImageFromUrl(view.context, view, url?:"")
        }



        @BindingAdapter("showDD")
        @JvmStatic fun showDD(view: TextView, date:String?){
            date?.let {
                val curSdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
                val sdf = SimpleDateFormat("dd-MMM-yyyy-HH:mm", Locale.getDefault())

                val curDate = curSdf.parse(it)
                val newDate = sdf.format(curDate)

                val split = newDate.split("-")
                view.text = split[0]
            }
        }

        @BindingAdapter("showMMHHmm")
        @JvmStatic fun showMMHHmm(view: TextView, date:String?){
            date?.let {
                val curSdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
                val sdf = SimpleDateFormat("dd-MMMM-yyyy-HH:mm", Locale.getDefault())

                val curDate = curSdf.parse(it)
                val newDate = sdf.format(curDate)

                val split = newDate.split("-")
                view.text = "${split[1]}\n${split[3]} WIB"
            }
        }

        @BindingAdapter("showddMMMMyyyy")
        @JvmStatic fun showddMMMMyyyy(view: TextView, date:String?){
            date?.let {
                val curSdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
                val sdf = SimpleDateFormat("dd MMMM yyyy HH:mm:ss", Locale.getDefault())

                val curDate = curSdf.parse(it)
                val newDate = sdf.format(curDate)

                val split = newDate.split("-")
                view.text = newDate
            }
        }




    }
}