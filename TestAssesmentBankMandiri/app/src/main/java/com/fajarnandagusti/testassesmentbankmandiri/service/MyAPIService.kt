package com.fajarnandagusti.testassesmentbankmandiri.service

import com.fajarnandagusti.testassesmentbankmandiri.model.ListArticles
import com.fajarnandagusti.testassesmentbankmandiri.model.ListSource
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface MyAPIService {

    @GET("top-headlines/sources?}")
    fun getSourceByCategory(@Query("apiKey") apiKey: String,
                            @Query("category") category: String,
                           ): Call<ListSource>

//    @GET("top-headlines?}")
//    fun getArticle(
//                    @Query("apiKey") apiKey: String,
//                    @Query("sources") sources: String,
//                    @Query("page") page: Int,
//                    @Query("pageSize") pageSize: Int,
//                    ): Call<ListArticles>

    @GET("everything?}")
    fun getArticle(

                    @Query("apiKey") apiKey: String,
                    @Query("sources") sources: String,
                    @Query("page") page: Int,
                    @Query("pageSize") pageSize: Int,
                    @Query("q") search: String,
                    ): Call<ListArticles>
}