package com.fajarnandagusti.testassesmentbankmandiri.base

interface OnPageInteraction {
    fun setTitlePage(title:String)
    fun addFragment(fragment:BaseFragment)
    fun onBackPressed(onback:OnBackPressListener)
}

interface OnBackPressListener{
    fun onBackPressed(activity: BaseActivity)
}