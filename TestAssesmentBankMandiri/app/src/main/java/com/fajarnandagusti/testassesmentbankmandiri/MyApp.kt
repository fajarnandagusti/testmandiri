package com.fajarnandagusti.testassesmentbankmandiri

import android.media.MediaDrm
import android.util.Log
import androidx.multidex.MultiDexApplication
import com.fajarnandagusti.testassesmentbankmandiri.base.BaseUI
import com.fajarnandagusti.testassesmentbankmandiri.service.MyAPIService
import com.google.gson.GsonBuilder
import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.lang.NumberFormatException
import java.security.MessageDigest
import java.util.*
import java.util.concurrent.TimeUnit

class MyApp : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()

//        Realm.init(this)
//        val config = RealmConfiguration.Builder()
//            .name("edc.realm")
//            .allowWritesOnUiThread(true)
//            .schemaVersion(1)
//            .deleteRealmIfMigrationNeeded()
//            .build()
//        Realm.setDefaultConfiguration(config)


    }

    companion object {

        /**
         * A native method that is implemented by the '' native library,
         * which is packaged with this application.
         */
        external fun getBaseUrl(isProduction: Boolean): String
        external fun getPathInfo(isProduction: Boolean): String


        /**
         * In this section, initial method for request to REST API.
         * I made the method function staticly.
         *
         * Retrofit 2.x
         */
        fun <T> callApi(baseUI: BaseUI, call: Call<T>, callback: Callback<T>) {
//            baseUI.progressBar?.visibility = View.VISIBLE
            call.enqueue(callback)
        }

        fun getApi(): MyAPIService {
            val retrofit = Retrofit.Builder()
                .baseUrl(com.fajarnandagusti.testassesmentbankmandiri.BuildConfig.HOST_URL)
                .client(getClient())
                .addConverterFactory(
                    GsonConverterFactory.create(
                        GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.SSS")
                            .registerTypeAdapter(Double::class.java, DoubleTypeAdapter())
                            .excludeFieldsWithoutExposeAnnotation()
                            .create()
                    )
                ).build()

            return retrofit.create(MyAPIService::class.java)
        }

        class DoubleTypeAdapter : TypeAdapter<Double>() {

            override fun write(writer: JsonWriter?, value: Double?) {
                try {
                    writer?.value(value)
                } catch (e: IOException) {
                    e.printStackTrace()
                    Log.e("DoubleTypeAdapter", "write: IO<exception>: ${e.message}")
                }
            }

            override fun read(`reader`: JsonReader?): Double? {

                return if (reader == null) {
                    null
                } else {
                    try {
                        if (reader.peek() == JsonToken.NULL) {
                            reader.nextNull()
                            null
                        } else {
                            reader.nextString().toDoubleOrNull()
                        }

                    } catch (e: IOException) {
                        e.printStackTrace()
                        Log.e("DoubleTypeAdapter", "read: IO<exception>: ${e.message}")
                        null
                    } catch (e: NumberFormatException) {
                        e.printStackTrace()
                        Log.e("DoubleTypeAdapter", "read: NumberFormat<exception>: ${e.message}")
                        null
                    }
                }
            }
        }

        private fun getClient(): OkHttpClient {

            try {

                val builder = OkHttpClient.Builder()
                return builder
                    .connectTimeout(5, TimeUnit.MINUTES)
                    .readTimeout(5, TimeUnit.MINUTES)
                    .writeTimeout(5, TimeUnit.MINUTES)
                    .addInterceptor(getLoggingInterceptor())
                    .build()
            } catch (e: Exception) {
                throw RuntimeException(e)
            }

        }

        private fun getLoggingInterceptor(): Interceptor {
            val httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.level = getInterceptorLevel()
            return httpLoggingInterceptor
        }

        private fun getInterceptorLevel(): HttpLoggingInterceptor.Level {
            return if (true)
                HttpLoggingInterceptor.Level.BODY
            else
                HttpLoggingInterceptor.Level.NONE
        }


    }
}