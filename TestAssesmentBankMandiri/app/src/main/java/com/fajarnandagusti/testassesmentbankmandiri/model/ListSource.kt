package com.fajarnandagusti.testassesmentbankmandiri.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ListSource : BaseModel() {
    @SerializedName("sources")
    @Expose
    val sources: List<Sources>? = null
}